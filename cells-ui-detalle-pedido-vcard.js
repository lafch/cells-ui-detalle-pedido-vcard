import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-detalle-pedido-vcard-styles.js';

import '@bbva-web-components/bbva-form-field';
import '@vcard-components/cells-util-behavior-vcard';

/**
This component ...

Example:

```html
<cells-ui-detalle-pedido-vcard></cells-ui-detalle-pedido-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const cellsUtilBehaviorVcard = CellsBehaviors.cellsUtilBehaviorVcard;

export class CellsUiDetallePedidoVcard extends cellsUtilBehaviorVcard(LitElement) {
  static get is() {
    return 'cells-ui-detalle-pedido-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      detallePedido : {type: Array},
      detallePedidoMostrar : {type: Array}
    };
  }

  // Initialize properties
  constructor() {
    super();
    
    this.detallePedido = [];
    this.detallePedidoMostrar = this.detallePedido;
   
   /* let pedido = new Object();

    let tarjeta = new Object()
    tarjeta.formato='ESTANDAR';
    tarjeta.bin='12345678';
    tarjeta.value='W3 - MASTERCARD INCHA';
    
    pedido.tarjeta = tarjeta;
    pedido.cantidad = '150';
    this.detallePedido.push(pedido);
    
    let pedido2= new Object();

    let tarjeta2 = new Object()
    tarjeta2.formato='STICKER';
    tarjeta2.bin='9999999';
    tarjeta2.value='V3 - MASTERCARD INCHA';
    
    pedido2.tarjeta = tarjeta2;
    pedido2.cantidad = '150';

    this.detallePedido.push(pedido2);
    this.detallePedido.push(pedido);

    this.detallePedidoMostrar = this.detallePedido;
    */
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-detalle-pedido-vcard-shared-styles').cssText}
    `;
  }

  cargarDetalle(detail){
    this.detallePedido = detail.details;
    this.detallePedidoMostrar = this.detallePedido;
    console.log(detail.details);
  }

  buscarTarjetaNombre(){
    let filterNombre = this.getInputValue('txtFilterNombre');
    if(filterNombre){
      this.detallePedidoMostrar = this.detallePedido.filter( pedido => { 
        return pedido.tarjeta.value.toUpperCase().indexOf(filterNombre.toUpperCase()) >= 0 || pedido.tarjeta.bin.toUpperCase().indexOf(filterNombre.toUpperCase()) >= 0 || pedido.tarjeta.formato.toUpperCase().indexOf(filterNombre.toUpperCase()) >= 0;
      });
    }else{
      this.detallePedidoMostrar = this.detallePedido;
    }
  }

  htmtDetallePedido(){
    return html`
      <div class='contenedor-filtro'>
        <div class='contenedor-filtro-titulo'><span> FILTRO POR DETALLE: </span></div>
        <bbva-form-field id='txtFilterNombre' label='Ingresar un detalle' @keyup='${()=> { this.buscarTarjetaNombre() }}' @click='${()=> { this.buscarTarjetaNombre() }}'></bbva-form-field> 
      </div>
      <div class='fila-detalle-header'>
        <div class='celda-detalle-header titulo-detalle'>DETALLE DE TARJETA </div>
        <div class='celda-cantidad-header titulo-cantidad'>CANTIDAD </div>
      </div>
      <div class='contenedor-detalle'>
        ${this.detallePedidoMostrar > 0 ? this.listarPedido(this.detallePedidoMostrar): this.listarPedido(this.detallePedido)}
      </div>
    `;
  }

  listarPedido(pedido){
    return html`${this.detallePedidoMostrar.map(pedido => html`
        ${this.htmlItemPedido(pedido)}
    `)}`;
  }

  htmlItemPedido(pedido){
    return html`<div class='fila-detalle'>
    <div class='celda-detalle'>
      <div class='celda-detalle-tarjeta'><span>TARJETA : ${pedido.tarjeta.value}<span></div>
      <div class='celda-detalle-bin'><span>BIN : ${pedido.tarjeta.bin}<span></div>
      <div class='celda-detalle-formato'><span>FORMATO : ${pedido.tarjeta.formato}<span></div>
    </div>
    <div class='celda-cantidad'>
      <div class='celda-cantidad-item'><span>${pedido.cantidad}<span></div>
    </div>
  </div>`;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <div class='contenedor'>
        ${this.htmtDetallePedido()}
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiDetallePedidoVcard.is, CellsUiDetallePedidoVcard);
